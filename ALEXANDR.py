import random

def main():
    while True:
        ex = input('Choose program (1 or 2, 0 to exit): ')

        if ex == '0':
            break
        elif ex == '1':
            num = input('Guess number: ')
            if num.isdigit(): num = int(num)

            if random.randint(1, 9) == num:
                print('You won!')
            else: 
                print('You lost!')
        elif ex == '2':
            nums = input('Type numbers (1 2 5): ')
            nums = nums.split(' ')
            a, b = 0, 0
            for n in nums:
                if n.isdigit():
                    n = int(n)
                    if n % 2: 
                        a += 1
                        print(n, end=' ')
                    else: b += 1

            
            print(f'\n{a} numbers are even and {b} numbers are odd')
            
        

if __name__ == '__main__':
    main()
